package com.dfn.rm.email.mailsender;

import com.dfn.rm.email.ContactMail;
import com.dfn.rm.email.mailconfig.MailConfig;

import javax.swing.*;

/**
 * Common Mail sender class .
 */

public class MailSenderSwingWorker extends SwingWorker<Void, Void> {
    protected ContactMail contactMail;

    public MailSenderSwingWorker(ContactMail contactMail) {
        this.contactMail = contactMail;
    }

    @Override
    protected Void doInBackground() throws Exception {
        MailConfig.sendPaperMarkedMail(contactMail);
        return null;
    }

    @Override
    protected void done() {

    }
}
