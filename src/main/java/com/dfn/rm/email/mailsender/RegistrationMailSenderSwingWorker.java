package com.dfn.rm.email.mailsender;

import com.dfn.rm.email.ContactMail;
import com.dfn.rm.email.mailconfig.MailConfig;

/**
 * Registration Mail sender class .
 */

public class RegistrationMailSenderSwingWorker extends MailSenderSwingWorker {

    public RegistrationMailSenderSwingWorker(ContactMail contactMail) {
        super(contactMail);
    }

    @Override
    protected Void doInBackground() throws Exception {
        MailConfig.sendUserRegistereddMail(contactMail);
        return null;
    }

    @Override
    protected void done() {

    }
}
