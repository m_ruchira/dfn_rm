package com.dfn.rm.email.mailconfig;

import com.dfn.rm.email.ContactMail;
import com.dfn.rm.email.MailProperties;
import com.dfn.rm.email.mailbuilder.PanelMailContentBuilder;
import com.dfn.rm.email.mailbuilder.RegistrationMailContentBuilder;
import com.dfn.rm.util.MailUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.mail.*;
import javax.mail.internet.*;
import java.util.Properties;

/**
 * Common Mail config for send mails.
 */

public class MailConfig {

    private static final Logger LOGGER = LogManager.getLogger(MailConfig.class);
    private static RegistrationMailContentBuilder registrationMailContentBuilder;
    private static PanelMailContentBuilder panelMailContentBuilder;

    /**
     * Method for send a Mail by including details coming with the argument
     * Email body is set as a html not as a text
     * This email can contains release code,username,pro version,images(SS) ,
     * sender Message(Text),sender's Name and contact detail.
     */
    public static void sendPaperMarkedMail(ContactMail contactMail) {

        Message message = new MimeMessage(getSessionObject());
        panelMailContentBuilder = new PanelMailContentBuilder();

        try {
            message.setSubject("IQ Paper_Marked");
            message.setFrom(new InternetAddress(MailProperties.FROM_ADDRESS));

            for (String email : contactMail.getEmailList()) {
                message.addRecipients(Message.RecipientType.CC, InternetAddress.parse(email));
            }

            MimeBodyPart messageBodyPart = new MimeBodyPart();
            MimeMultipart multipart = new MimeMultipart();

            String htmlText = panelMailContentBuilder.build();

            htmlText = MailUtils.replacePanelFields(htmlText, contactMail);

            messageBodyPart.setContent(htmlText, "text/html;charset=utf-8");
            multipart.addBodyPart(messageBodyPart);
            message.setContent(multipart);

            // Send message
            Transport.send(message);
        } catch (MessagingException e) {
            LOGGER.error(e);
        } catch (Exception e) {
            LOGGER.error(e);
        }

    }

    public static void sendUserRegistereddMail(ContactMail contactMail) {

        Message message = new MimeMessage(getSessionObject());
        registrationMailContentBuilder = new RegistrationMailContentBuilder();

        try {
            message.setSubject("Final Interview - Internship JAVA");
            message.setFrom(new InternetAddress(MailProperties.FROM_ADDRESS));

            for (String email : contactMail.getEmailList()) {
                message.addRecipients(Message.RecipientType.CC, InternetAddress.parse(email));
            }

            MimeBodyPart messageBodyPart = new MimeBodyPart();
            MimeMultipart multipart = new MimeMultipart();

            String htmlText = registrationMailContentBuilder.build();

            htmlText = MailUtils.replaceRegistrationFields(htmlText, contactMail);

            messageBodyPart.setContent(htmlText, "text/html;charset=utf-8");
            multipart.addBodyPart(messageBodyPart);
            message.setContent(multipart);

            // Send message
            Transport.send(message);
        } catch (MessagingException e) {
            LOGGER.error(e);
        } catch (Exception e) {
            LOGGER.error(e);
        }

    }

    public Object getHTMLBody() {
        return "";
    }


    /**
     * Give a session object relevant to the mail server.
     */
    public static Session getSessionObject() {
        Properties props = System.getProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", MailProperties.IS_AUTH_ENABLED);
        props.put("mail.smtp.starttls.enable", MailProperties.IS_TLS_ENABLED);
        props.put("mail.smtp.host", MailProperties.SMTP_HOST);
        props.put("mail.smtp.port", MailProperties.PORT);

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(MailProperties.USER_NAME, MailProperties.PASSWORD);

                    }
                });
        return session;
    }


    /**
     * According the version Receiving mail address should be changed
     * This method return the relevant mail addresses for the version.
     */
    public static String getReceivingAddress() throws AddressException {
        return null;
    }

}
