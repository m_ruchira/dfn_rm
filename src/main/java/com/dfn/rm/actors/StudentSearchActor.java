package com.dfn.rm.actors;

import akka.japi.pf.ReceiveBuilder;
import com.dfn.rm.actors.messages.StartSearch;
import com.dfn.rm.beans.*;
import com.dfn.rm.connector.database.DBHandler;
import com.dfn.rm.connector.database.DBUtilStore;
import com.dfn.rm.connector.websocket.MessageType;
import com.dfn.rm.connector.websocket.events.SessionInfo;
import com.dfn.rm.connector.websocket.messages.RequestMessage;
import com.dfn.rm.connector.websocket.messages.ResponseHeader;
import com.dfn.rm.connector.websocket.messages.ResponseMessage;
import com.dfn.rm.util.Constants;
import com.dfn.rm.util.Utils;
import com.dfn.rm.util.json.JSONParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdbi.v3.core.Handle;

import java.util.List;

/**
 * This actor is responsible for a handling student search operations while working as a child of client actor.once
 * client notifies about the event.
 */

public class StudentSearchActor extends ClientActor {

    private static final Logger LOGGER = LogManager.getLogger(StudentSearchActor.class);

    public StudentSearchActor(SessionInfo sessionInfo) {
        super(sessionInfo);
    }

    @Override
    public Receive createReceive() {
        return ReceiveBuilder.create()
                .match(RequestMessage.class, this::handle)
                .match(StartSearch.class, this::handle)
                .build();
    }

    private void handle(StartSearch startSearch) {
        LOGGER.trace(" Start Search Messsage Passed Inside Student Search Actor "
                + JSONParser.getInstance().toJson(startSearch));
        try (Handle handle = DBUtilStore.getInstance().getDbiInstance().open()) {
            List<StudentSearchRecord> searchRecordList = DBHandler.getInstance().searchByPagination(
                    startSearch, handle);
            StudentSearch studentSearch = new StudentSearch();
            studentSearch.setRecordList(searchRecordList);
            sendMessage(studentSearch, MessageType.RESPONSE_MSG_STUDENT_SEARCH_BY_PAGINATION);
        } catch (Exception e) {
            LOGGER.error(e);
        }
    }


    private void handle(RequestMessage requestMessage) {
        LOGGER.trace(" Requesth Messsage Passed Inside Student Search Actor "
                + JSONParser.getInstance().toJson(requestMessage));
        try (Handle handle = DBUtilStore.getInstance().getDbiInstance().open()) {
            StartSearch startSearch = Utils.getMessageInstance(requestMessage.getData(), StartSearch.class);
            List<StudentSearchRecord> searchRecordList = DBHandler.getInstance().studentSearch(startSearch, handle);
            sendMessage(new RecordCount(searchRecordList.size()), MessageType.RESPONSE_MSG_STUDENT_SEARCH_INITIATED);

            List<StudentSearchRecord> recordSubList =
                    searchRecordList.size() < 10 ? searchRecordList
                            : searchRecordList.subList(0, Constants.SEARCH_COUNT_PER_PAGE);
            StudentSearch studentSearch = new StudentSearch();
            studentSearch.setRecordList(recordSubList);
            sendMessage(studentSearch, MessageType.RESPONSE_MSG_STUDENT_SEARCH_BY_PAGINATION);
        } catch (Exception e) {
            LOGGER.error(e);
        }
    }

    private void sendMessage(Object messege, int messageType) {
        ResponseMessage responseMessage = new ResponseMessage();
        ResponseHeader responseHeader = new ResponseHeader();
        responseHeader.setUserId(sessionInfo.getUserID());
        responseHeader.setSessionId(sessionInfo.getSessionID());
        responseHeader.setMessageType(messageType);
        if (messege != null) {
            responseMessage.setResObject(messege);
        }
        responseMessage.setHeader(responseHeader);

        try {
            LOGGER.trace(JSONParser.getInstance().toJson(responseMessage));
            getContext().getParent().tell(responseMessage, getSelf());
        } catch (Exception e) {
            LOGGER.error("Error {}", e, e);
        }
    }

}