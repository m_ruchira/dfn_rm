package com.dfn.rm.actors.messages;

/**
 * Message Class PauseActiveExam Passing across Actors .
 */

public class PauseActiveExam {

    private String userID;

    public PauseActiveExam(String userID) {
        this.userID = userID;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

}
