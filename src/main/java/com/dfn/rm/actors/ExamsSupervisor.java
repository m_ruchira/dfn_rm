package com.dfn.rm.actors;

import akka.actor.*;
import akka.japi.pf.DeciderBuilder;
import akka.japi.pf.ReceiveBuilder;
import com.dfn.rm.actors.messages.ExamStart;
import com.dfn.rm.actors.messages.PauseActiveExam;
import com.dfn.rm.actors.messages.ScheduleExam;
import com.dfn.rm.beans.ExamDetails;
import com.dfn.rm.connector.database.DBHandler;
import com.dfn.rm.connector.database.DBUtilStore;
import com.dfn.rm.connector.websocket.messages.LoginResponse;
import com.dfn.rm.connector.websocket.messages.RequestMessage;
import com.dfn.rm.util.ActorName;
import com.dfn.rm.util.ActorUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdbi.v3.core.Handle;

import java.time.Duration;
import java.util.List;

import static akka.actor.SupervisorStrategy.resume;

/**
 * ExamSupervisor will be responsible for creating new Exam actors
 * which is responsible for handling exam operations. When ever the client
 * supervisor ask the exam supervisor to pass the specific message to exam actor which
 * relates to a specific client the examsupervisor checks whether specific exam actor exists
 * for that message using userId and examId if not examsupervisor creates new actor using
 * userId and examId.
 */

public class ExamsSupervisor extends AbstractActor {

    private static final Logger LOGGER = LogManager.getLogger(ExamsSupervisor.class);
    private static final int MAX_NO_OF_RETRIES = 1;
    private static final int DURATION = 8;
    private static SupervisorStrategy strategy =
            new OneForOneStrategy(
                    MAX_NO_OF_RETRIES,
                    Duration.ofSeconds(DURATION),
                    DeciderBuilder.match(Exception.class, e -> resume())
                            .build());


    @Override
    public void preStart() throws Exception {
        super.preStart();
        LOGGER.info("Starting Exam Supervisor");

    }

    @Override
    public SupervisorStrategy supervisorStrategy() {
        return strategy;
    }

    @Override
    public Receive createReceive() {
        return ReceiveBuilder.create()
                .match(ExamStart.class, this::handle)
                .match(PauseActiveExam.class, this::handle)
                .match(RequestMessage.class, this::handle)
                .match(LoginResponse.class, this::handle)
                .build();
    }

    private void handleEvent(String userID, Object message) {
        try (Handle handle = DBUtilStore.getInstance().getDbiInstance().open()) {
            List<ExamDetails> examDetailsList = DBHandler.getInstance().getExamDetails(
                    userID, handle
            );
            if (examDetailsList.size() > 0) {
                ExamDetails examDetails = examDetailsList.get(0);
                sendToExamActor(examDetails.getExamId(), userID, message);
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
    }

    private void handle(PauseActiveExam pauseActiveExam) {
        handleEvent(pauseActiveExam.getUserID(), pauseActiveExam);
    }

    private void handle(RequestMessage requestMessage) {
        handleEvent(requestMessage.getHeader().getUserId(), requestMessage);
    }

    private void handle(LoginResponse loginResponse) {
        try {
            ScheduleExam scheduleExam = loginResponse.getScheduleExam();
            ActorRef examActor = ActorUtils.getChildActor(getContext(),
                    ActorName.EXAM_ACTOR,
                    scheduleExam.getExamId()
                            + "-"
                            + scheduleExam.getUserId());
            if (examActor == null) {
                examActor = getContext().actorOf(Props.create(ExamActor.class, loginResponse)
                                .withDispatcher("client-dispatcher"),
                        ActorName.EXAM_ACTOR.toString() + "-" + scheduleExam.getExamId()
                                + "-" + scheduleExam.getUserId());
            }
            examActor.tell(loginResponse, getSender());
        } catch (Exception e) {
            LOGGER.error("Error {}", e, e);
        }
    }

    private void handle(ExamStart examStart) {
        try {
            sendToExamActor(examStart.getExamId(), examStart.getUserId(), examStart);
        } catch (Exception e) {
            LOGGER.error(e);
        }
    }

    private void sendToExamActor(long examId, String userId, Object message) {
        ActorRef examActor = ActorUtils.getChildActor(getContext(),
                ActorName.EXAM_ACTOR,
                examId + "-" + userId);
        if (examActor != null) {
            examActor.tell(message, getSender());
        } else {
            LOGGER.error("No exam actor found for message: {}, EID {}, USR {}", message, examId, userId);
        }
    }
}
