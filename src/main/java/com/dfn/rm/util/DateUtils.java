package com.dfn.rm.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * date Utility class for performing various Date util actions.
 */

public final class DateUtils {

    private static final Logger LOGGER = LogManager.getLogger(DateUtils.class);

    private static final Locale LOCALE = new Locale("fr", "FR");
    private static final DateFormat TIMER_FORMAT = DateFormat.getTimeInstance(DateFormat.DEFAULT, LOCALE);
    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    private static final int MINUTES_TO_SECONDS = 60;
    private static final int HOURS_TO_SECONDS = 3600;

    private DateUtils() {

    }

    public static String generateTime(long duration) {
        long hour = duration / HOURS_TO_SECONDS;
        duration %= HOURS_TO_SECONDS;

        long minutes = duration / MINUTES_TO_SECONDS;
        duration %= MINUTES_TO_SECONDS;

        long seconds = duration;

        return convertToDateFormat(hour, minutes, seconds);
    }

    private static String convertToDateFormat(long hours, long minutes, long seconds) {
        Calendar now = Calendar.getInstance();
        now.set(Calendar.HOUR_OF_DAY, (int) hours);
        now.set(Calendar.MINUTE, (int) minutes);
        now.set(Calendar.SECOND, (int) seconds);
        return TIMER_FORMAT.format(now.getTime());
    }

    public static String generateDuration(long duration) {
        long hour = duration / MINUTES_TO_SECONDS;
        duration %= MINUTES_TO_SECONDS;

        long minutes = duration;

        return hour > 0 ? hour + " hour " + (minutes > 0 ? minutes + " minutes. " : "")
                : minutes > 0 ? minutes + " minutes." : "";

    }

    public static long convertToSeconds(long minutes) {
        return minutes * MINUTES_TO_SECONDS;
    }

    public static String formatDateField(String fromDate, String toDate) {
        if (fromDate.trim().isEmpty()) {
            return toDate.trim().isEmpty() ? null : toDate.trim();
        } else {
            return toDate.trim().isEmpty() ? DATE_FORMAT.format(new Date()) : toDate.trim();
        }
    }

}
