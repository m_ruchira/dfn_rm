package com.dfn.rm.util;

/**
 * Constants class of Recrutement Manager.
 */

public final class Constants {

    public static final int USER_CAT_ADMIN = 1;
    public static final int USER_CAT_EXAM = 0;

    public static final int LOGIN_SUCCESS = 1;
    public static final int LOGIN_FAILED = 0;
    public static final int LOGIN_RESTRICTED = 2;
    public static final int LOGIN_EXPIRED = 3;

    public static final String DEF_EXAM_DESCRIPTION = "EXAM DESCRIPTION";
    public static final String DEF_EXAM_HONOR_CODE = "EXAM HONOR CODE";
    public static final int DURATION = 60;

    public static final int OFF_DURATION = 1;
    public static final int ON_DURATION = 0;

    public static final int AUTO_GENERATE = 0;
    public static final int PERSISTED_GENERATE = 1;

    public static final int EXAM_RESTRICTED = 3;
    public static final int EXAM_ON_HOLD = 0;
    public static final int EXAM_STARTED = 1;
    public static final int EXAM_FINISHED = 2;

    public static final int EXAM_CATEGORY_IQ = 0;
    public static final int EXAM_CATEGORY_TECHNICAL = 1;

    public static final int QUESTION_TYPE_MCQ = 0;
    public static final int QUESTION_TYPE_SHORT_ANSWERS = 1;

    public static final int IQ_QUESTIONS_COUNT = 20;

    public static final String POSITION_INTERN = "INTERN";
    public static final String POSITION_SE = "SOFTWARE ENGINEER";
    public static final String POSITION_SSE = "SENIOR SOFTWARE ENGINEER";
    public static final String POSITION_ATL = "ASSOCIATE TECH LEAD";
    public static final String POSITION_TL = "TECH LEAD";
    public static final String POSITION_STL = "Senior TECH LEAD";

    public static final int DIFFICULTY_CONFIG_EASY = 0;
    public static final int DIFFICULTY_CONFIG_MEDIUM = 1;
    public static final int DIFFICULTY_CONFIG_HARD = 2;

    public static final int SEARCH_COUNT_PER_PAGE = 10;

    private Constants() {

    }
}
