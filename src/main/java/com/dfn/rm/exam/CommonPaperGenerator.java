package com.dfn.rm.exam;

import com.dfn.rm.beans.Answer;
import com.dfn.rm.beans.ExamPaper;
import com.dfn.rm.beans.Question;
import com.dfn.rm.connector.database.DBHandler;
import com.dfn.rm.connector.database.DBUtilStore;
import com.dfn.rm.util.PaperUtils;
import org.jdbi.v3.core.Handle;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

import static com.dfn.rm.util.Constants.*;

/**
 * Common Exam paper Generator .
 */

public class CommonPaperGenerator implements ExamPaperGenerator {

    protected int examCategory;
    private int iqQuestions;
    protected ExamPaper examPaper;

    protected static final ThreadLocalRandom RANDOM_NUMBER_GENERATOR = ThreadLocalRandom.current();


    public CommonPaperGenerator(int examCategory) {
        this.examCategory = examCategory;
        examPaper = new ExamPaper(new ArrayList<>());
    }

    @Override
    public ExamPaper generate() {
        generateExamPaper(examCategory);
        return getExamPaper();
    }


    private void generateExamPaper(int examCategory) {
        switch (examCategory) {
            case EXAM_CATEGORY_IQ:
                generateIQPaper();
                break;
            case EXAM_CATEGORY_TECHNICAL:
                generateMixPaper();
                break;
            default:
                break;
        }
    }

    protected List<Question> generateIQQuestions() {
        List<Question> generatedIQQuestions = new ArrayList<>();
        try (Handle handle = DBUtilStore.getInstance().getDbiInstance().open()) {
            List<Question> iqQuestionsList = DBHandler.getInstance().getQuestions(handle);
            List<Answer> allAnswers = DBHandler.getInstance().getAllAnswers(handle);
            for (int i = 0; i < IQ_QUESTIONS_COUNT; i++) {
                int id = RANDOM_NUMBER_GENERATOR.nextInt(iqQuestionsList.size());
                Question question = iqQuestionsList.get(id);
                List<Answer> answersList = allAnswers.stream().filter(
                        x -> x.getQuestionId() == question.getId()).collect(Collectors.toList());
                if (question.getCategory() == QUESTION_TYPE_SHORT_ANSWERS) {
                    answersList = PaperUtils.filteredAnswerList(answersList);
                }
                if (answersList != null) {
                    question.setAnswer(answersList);
                }
                generatedIQQuestions.add(question);
                iqQuestionsList.remove(id);
            }
        } catch (Exception e) {

        }
        return generatedIQQuestions;
    }


    public ExamPaper generateMixPaper() {
        return null;
    }

    public ExamPaper generateIQPaper() {
        List<Question> questions = generateIQQuestions();
        examPaper.setQuestions(questions);
        return examPaper;
    }


    private void generateDifficultQues() {

    }

    public int getIqQuestions() {
        return iqQuestions;
    }

    public void setIqQuestions(int iqQuestions) {
        this.iqQuestions = iqQuestions;
    }

    public ExamPaper getExamPaper() {
        return examPaper;
    }
}
