package com.dfn.rm.exam;

import com.dfn.rm.beans.ExamPaper;
import com.dfn.rm.util.Constants;

/**
 * Factory class to generate differnt exam papers .
 */

public class ExamPaperFactory {

    private ExamPaperGenerator iExamPaperGenerator;

    public ExamPaperFactory(int examType, String position, int examCategory) {
        if (examType == Constants.AUTO_GENERATE) {
            if (examCategory == Constants.EXAM_CATEGORY_IQ) {
                iExamPaperGenerator = new CommonPaperGenerator(examCategory);
            } else if (examCategory == Constants.EXAM_CATEGORY_TECHNICAL) {
                iExamPaperGenerator = new MixPaperGenerator(position, examCategory);
            }
        }
    }

    public ExamPaper generateExamPaper() {
        return iExamPaperGenerator.generate();

    }

}
