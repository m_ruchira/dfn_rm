package com.dfn.rm.beans;

import java.io.Serializable;
import java.util.List;

/**
 * ExamPaper class used to store generated ExamPaper.
 */

public class ExamPaper implements Serializable {

    private static final long serialVersionUID = 30L;

    private List<Question> questions;

    public ExamPaper(List<Question> questions) {
        this.questions = questions;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }
}
