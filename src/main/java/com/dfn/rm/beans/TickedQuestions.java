package com.dfn.rm.beans;

import java.util.List;

/**
 * TickedQuestion class used to generate MarkedExamPaper.
 */

public class TickedQuestions {
    private int questionID;

    private List<String> answerID;

    public int getQuestionID() {
        return questionID;
    }

    public void setQuestionID(int questionID) {
        this.questionID = questionID;
    }

    public List<String> getAnswerIDs() {
        return answerID;
    }

    public void setAnswerIDs(List<String> answerID) {
        this.answerID = answerID;
    }
}
