package com.dfn.rm.beans;

/**
 * ExamActorRecovery class used to store snapshot of ExamActor.
 */

public class ExamActorRecovery {

    private int id;
    private String userID;
    private String paperID;
    private long examID;
    private long elapsedTime;
    private int overlyElapsed;
    private byte[] paperRecoveryByte;


    public ExamActorRecovery() {

    }

    public ExamActorRecovery(String userID, String paperID, long examID, long elapsedTime,
                             int overlyElapsed, byte[] paperRecoveryByte) {
        this.userID = userID;
        this.paperID = paperID;
        this.examID = examID;
        this.elapsedTime = elapsedTime;
        this.overlyElapsed = overlyElapsed;
        this.paperRecoveryByte = paperRecoveryByte;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getPaperID() {
        return paperID;
    }

    public void setPaperID(String paperID) {
        this.paperID = paperID;
    }

    public long getExamID() {
        return examID;
    }

    public void setExamID(long examID) {
        this.examID = examID;
    }

    public byte[] getPaperRecoveryByte() {
        return paperRecoveryByte;
    }

    public void setPaperRecoveryByte(byte[] paperRecoveryByte) {
        this.paperRecoveryByte = paperRecoveryByte;
    }

    public long getElapsedTime() {
        return elapsedTime;
    }

    public void setElapsedTime(long elapsedTime) {
        this.elapsedTime = elapsedTime;
    }

    public int getOverlyElapsed() {
        return overlyElapsed;
    }

    public void setOverlyElapsed(int overlyElapsed) {
        this.overlyElapsed = overlyElapsed;
    }
}
