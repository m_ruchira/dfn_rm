package com.dfn.rm.beans;

/**
 * ExamAnswer class used to add Answers to DB .
 */

public class ExamAnswer {

    private String userID;
    private String paperID;
    private int questionID;
    private String answerID;

    public ExamAnswer(String userID, String paperID, int questionID, String answerID) {
        this.userID = userID;
        this.paperID = paperID;
        this.questionID = questionID;
        this.answerID = answerID;
    }

    public ExamAnswer() {

    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getPaperID() {
        return paperID;
    }

    public void setPaperID(String paperID) {
        this.paperID = paperID;
    }

    public int getQuestionID() {
        return questionID;
    }

    public void setQuestionID(int questionID) {
        this.questionID = questionID;
    }

    public String getAnswerID() {
        return answerID;
    }

    public void setAnswerID(String answerID) {
        this.answerID = answerID;
    }
}
