package com.dfn.rm.beans;

import java.util.List;

/**
 * Student Search Class used to send student search record List for each search request.
 */

public class StudentSearch {

    List<StudentSearchRecord> recordList;

    public List<StudentSearchRecord> getRecordList() {
        return recordList;
    }

    public void setRecordList(List<StudentSearchRecord> recordList) {
        this.recordList = recordList;
    }
}
