package com.dfn.rm.beans;

/**
 * CVStatus class used to store cv upload status.
 */

public class CVStatus {

    private boolean status;

    public CVStatus(boolean status) {
        this.status = status;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
