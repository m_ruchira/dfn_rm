package com.dfn.rm.beans;

import com.google.gson.annotations.SerializedName;

/**
 * UserRegistration class used to create new Users and add to DB.
 */

public class UserRegistration {

    private long id;
    @SerializedName("nic")
    private String nic;
    @SerializedName("fullName")
    private String name;
    @SerializedName("department")
    private String department;
    @SerializedName("email")
    private String email;
    @SerializedName("mobile")
    private String mobile;
    @SerializedName("university")
    private String univeristy;
    @SerializedName("position")
    private String position;
    @SerializedName("cv")
    private String cv;
    @SerializedName("date")
    private String techIntviewDate;
    @SerializedName("time")
    private String techIntviewTime;
    @SerializedName("panel")
    private String techIntPanel;

    public UserRegistration() {

    }


    public UserRegistration(String nic, String name, String department, String email,
                            String mobile, String univeristy, String position, String cv,
                            String techIntviewDate, String techIntviewTime, String techIntPanel) {
        setNic(nic);
        setDepartment(department);
        setName(name);
        setEmail(email);
        setMobile(mobile);
        setUniveristy(univeristy);
        setPosition(position);
        setCv(cv);
        setTechIntviewDate(techIntviewDate);
        setTechIntviewTime(techIntviewTime);
        setTechIntvietPanel(techIntPanel);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getUniveristy() {
        return univeristy;
    }

    public void setUniveristy(String univeristy) {
        this.univeristy = univeristy;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getCv() {
        return cv;
    }

    public void setCv(String cv) {
        this.cv = cv;
    }

    public String getTechIntviewDate() {
        return techIntviewDate;
    }

    public void setTechIntviewDate(String techIntviewDate) {
        this.techIntviewDate = techIntviewDate;
    }

    public String getTechIntviewPanel() {
        return techIntPanel;
    }

    public void setTechIntvietPanel(String techIntPanel) {
        this.techIntPanel = techIntPanel;
    }

    public String getTechIntviewTime() {
        return techIntviewTime;
    }

    public void setTechIntviewTime(String techIntviewTime) {
        this.techIntviewTime = techIntviewTime;
    }
}
