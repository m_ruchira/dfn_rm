package com.dfn.rm.connector.websocket.messages;

import com.google.gson.annotations.SerializedName;
import org.eclipse.jetty.http.HttpStatus;

/**
 * Created by manodyas on 9/8/2017.
 */
public class ResponseHeader {
    @SerializedName("msgTyp")
    private int messageType;
    @SerializedName("channel")
    private int channelId;
    @SerializedName("commVer")
    private String clientVersion;
    @SerializedName("loginId")
    private String userId;
    @SerializedName("sesnId")
    private String sessionId;
    @SerializedName("tenantCode")
    private String tenantCode;
    @SerializedName("clientIp")
    private String clientIp;
    @SerializedName("unqReqId")
    private String unqReqId;
    @SerializedName("routeId")
    private int routeId;
    @SerializedName("status")
    private int status;

    public static ResponseHeader getNewInstance(int messageGroup, int channelId, String clientVersion, String userId,
                                                String sessionId) {
        ResponseHeader responseHeader = new ResponseHeader();
        responseHeader.setMessageType(messageGroup);
        responseHeader.setChannelId(channelId);
        responseHeader.setClientVersion(clientVersion);
        responseHeader.setUserId(userId);
        responseHeader.setSessionId(sessionId);
        return responseHeader;
    }

    public static ResponseHeader getNewInstance(RequestHeader requestHeader) {
        ResponseHeader responseHeader = new ResponseHeader();
        responseHeader.setMessageType(requestHeader.getMessageType());
        responseHeader.setChannelId(requestHeader.getChannelId());
        responseHeader.setClientVersion(requestHeader.getClientVersion());
        responseHeader.setUserId(requestHeader.getUserId());
        responseHeader.setSessionId(requestHeader.getSessionId());
        responseHeader.setClientIp(requestHeader.getClientIp());
        responseHeader.setTenantCode(requestHeader.getTenantCode());
        responseHeader.setRouteId(0);
        responseHeader.setUnqReqId(requestHeader.getUnqReqId());
        responseHeader.setStatus(HttpStatus.OK_200);
        return responseHeader;
    }

    public int getMessageType() {
        return messageType;
    }

    public void setMessageType(int messageType) {
        this.messageType = messageType;
    }

    public int getChannelId() {
        return channelId;
    }

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }

    public String getClientVersion() {
        return clientVersion;
    }

    public void setClientVersion(String clientVersion) {
        this.clientVersion = clientVersion;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public String getClientIp() {
        return clientIp;
    }

    public void setClientIp(String clientIp) {
        this.clientIp = clientIp;
    }

    public String getUnqReqId() {
        return unqReqId;
    }

    public void setUnqReqId(String unqReqId) {
        this.unqReqId = unqReqId;
    }

    public int getRouteId() {
        return routeId;
    }

    public void setRouteId(int routeId) {
        this.routeId = routeId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
