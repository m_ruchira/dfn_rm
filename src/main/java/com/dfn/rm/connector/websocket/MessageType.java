package com.dfn.rm.connector.websocket;


/**
 * Message Types for Requests and Responses.
 */

public final class MessageType {


    public static final int REQUEST_MSG_PULSE = 3;
    public static final int REQUEST_MSG_LOGIN = 1;

    public static final int REQUEST_MSG_REGISTRATION_CONFIRMATTION = 102;

    public static final int REQUEST_MSG_START_EXAM = 50;
    public static final int REQUEST_MSG_MARK_PAPER = 53;

    public static final int REQUEST_MSG_CACHING_EXAM_ANSWERS = 70;

    public static final int REQUEST_MSG_CV_UPLOAD = 30;

    public static final int REQUEST_MSG_INITIATE_STUDENT_SEARCH = 150;
    public static final int REQUEST_MSG_STUDENT_SEARCH_BY_PAGINATION = 155;
    public static final int REQUEST_MSG_SHOW_EXAM_PAPER = 160;


    public static final int RESPONSE_MSG_LOGIN = 1;
    public static final int RESPONSE_MSG_LOGIN_SECONDARY = 2;

    public static final int RESPONSE_MSG_CV_UPLOAD = 30;

    public static final int RESPONSE_MSG_EXAM_TIME_REMAIN = 51;
    public static final int RESPONSE_MSG_EXAM_PAPER_GENERATED = 52;
    public static final int RESPONSE_MSG_EXAM_END = 53;

    public static final int RESPONSE_MSG_MARKED_EXAM_QUESTIONS = 80;

    public static final int RESPONSE_MSG_REGISTRATION_CONFIRMATTION = 102;

    public static final int RESPONSE_MSG_STUDENT_SEARCH_INITIATED = 150;
    public static final int RESPONSE_MSG_STUDENT_SEARCH_BY_PAGINATION = 155;
    public static final int RESPONSE_MSG_SHOW_EXAM_PAPER = 160;

    private MessageType() {

    }

}
