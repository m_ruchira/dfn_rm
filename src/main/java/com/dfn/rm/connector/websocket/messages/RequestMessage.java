package com.dfn.rm.connector.websocket.messages;

import com.google.gson.annotations.SerializedName;

/**
 * Created by manodyas on 9/8/2017.
 */
public class RequestMessage {

    @SerializedName("HED")
    private RequestHeader requestHeader;

    @SerializedName("DAT")
    private Object data;

    public RequestHeader getHeader() {
        return requestHeader;
    }

    public void setHeader(RequestHeader requestHeader) {
        this.requestHeader = requestHeader;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
