package com.dfn.rm.connector.websocket.messages;

import com.google.gson.annotations.SerializedName;

/**
 * Status response for requests that return a status.
 */
public class StatusResponse {
    @SerializedName("respSts")
    private int responseStatus = FAIL;

    public static final int SUCCESS = 1;
    public static final int FAIL = 0;

    public int getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(int responseStatus) {
        this.responseStatus = responseStatus;
    }
}
