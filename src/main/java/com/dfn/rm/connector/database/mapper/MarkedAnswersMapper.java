package com.dfn.rm.connector.database.mapper;

import com.dfn.rm.beans.ExamAnswer;
import org.jdbi.v3.core.mapper.RowMapper;


import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * DB Resultset mapper for marked answer information.
 */

public class MarkedAnswersMapper implements RowMapper<ExamAnswer> {

    @Override
    public ExamAnswer map(ResultSet resultSet, StatementContext statementContext)
            throws SQLException {
        ExamAnswer examAnswer = new ExamAnswer();
        examAnswer.setUserID(resultSet.getString("USER_ID"));
        examAnswer.setPaperID(resultSet.getString("PAPER_ID"));
        examAnswer.setAnswerID(resultSet.getString("ANSWER"));
        examAnswer.setQuestionID(resultSet.getInt("QUESTION_ID"));
        return examAnswer;
    }
}
