package com.dfn.rm.connector.database.mapper;

import com.dfn.rm.beans.StudentSearchRecord;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * DB Resultset mapper for student  search  information.
 */

public class StudentSearchMapper implements RowMapper<StudentSearchRecord> {

    @Override
    public StudentSearchRecord map(ResultSet resultSet, StatementContext statementContext)
            throws SQLException {
        StudentSearchRecord studentSearchRecord = new StudentSearchRecord();

        studentSearchRecord.setNic(resultSet.getString("NIC"));
        studentSearchRecord.setUserId(resultSet.getString("USER_ID"));
        studentSearchRecord.setName(resultSet.getString("NAME"));
        studentSearchRecord.setDepartment(resultSet.getString("DEPARTMENT"));
        studentSearchRecord.setEmail(resultSet.getString("EMAIL"));
        studentSearchRecord.setMobile(resultSet.getString("MOBILE"));
        studentSearchRecord.setUniversity(resultSet.getString("UNIVERSITY"));
        studentSearchRecord.setPosition(resultSet.getString("POSITION"));
        studentSearchRecord.setCvLink(resultSet.getString("CV"));
        studentSearchRecord.setIqPaperDate(resultSet.getString("TECH_INTERVIEW_DATE"));
        studentSearchRecord.setMarks(resultSet.getDouble("MARKS"));
        studentSearchRecord.setPaperLink(resultSet.getString("PAPER"));

        return studentSearchRecord;
    }
}
