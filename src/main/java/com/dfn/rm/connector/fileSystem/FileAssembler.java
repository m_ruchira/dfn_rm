package com.dfn.rm.connector.fileSystem;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

/**
 * File Assembler used to concatanate chunks of bytes and store cv in filesystem.
 */

public class FileAssembler {

    private static final Logger LOGGER = LogManager.getLogger(FileAssembler.class);

    private Map<Integer, byte[]> bytesMap;
    private File file;
    private String path;
    boolean finished = false;
    private FileOutputStream fileOutputStream;
    private final long totalByteCount;
    private final int totalChunkCount;
    private long totalBytes;
    private int totalChunks;

    public FileAssembler(String name, long totalByteCount, int chunkCount) {
        bytesMap = new TreeMap<>();
        this.totalByteCount = totalByteCount;
        this.totalChunkCount = chunkCount;
        path = FileSystem.getCVFilePathByName(name);
        file = new File(path);
    }

    public void addBytes(byte[] bytes, int index) {
        try {
            totalBytes += bytes.length;
            ++totalChunks;
            if (totalBytes == totalByteCount || totalChunks == totalChunkCount) {
                finished = true;
            }
            bytesMap.put(index, bytes);
        } catch (Exception e) {
            LOGGER.error(e);
        }

    }

    public boolean isFinished() {
        return finished;
    }

    public boolean createFile() {

        try {
            Iterator<Map.Entry<Integer, byte[]>> iterator = bytesMap.entrySet().iterator();
            fileOutputStream = new FileOutputStream(file);
            while (iterator.hasNext()) {
                Map.Entry<Integer, byte[]> entry = iterator.next();
                fileOutputStream.write(entry.getValue());
            }
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (IOException e) {
            LOGGER.error(e);
            return false;
        }
        return true;
    }
}
