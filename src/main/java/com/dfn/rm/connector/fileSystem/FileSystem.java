package com.dfn.rm.connector.fileSystem;

import com.dfn.rm.util.settings.Settings;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;


/**
 * This is the Class used to generate File System to store files.
 */

public final class FileSystem {

    private static final FileSystem INSTANCE = new FileSystem();

    private static final String CV_PATH = "filesystem/UserRegistration/CV";
    private static final String PAPER_PATH = "filesystem/exampaper/user/paper";

    public static FileSystem getInstance() {
        return INSTANCE;
    }

    private FileSystem() {

    }

    public static String getCVFileSystemPath() {
        return CV_PATH;
    }

    public static String getPaperFileSystemPath() {
        return PAPER_PATH;
    }

    public static void saveFile(String fileName, byte[] bytes) {
        try {
            String path = getCVFilePathByName(fileName);
            File file = new File(path);
            FileUtils.writeByteArrayToFile(file, bytes);
        } catch (IOException e) {

        }
    }

    public static void deleteFile(String fileName) {
        try {
            String path = getCVFilePathByName(fileName);
            FileUtils.touch(new File(path));
            File file = FileUtils.getFile(path);
            FileUtils.deleteQuietly(file);
        } catch (IOException e) {

        }
    }

    public static void renameFile(String oldFileName, String newFileName) {
        try {
            FileUtils.moveFile(
                    FileUtils.getFile(getCVFilePathByName(oldFileName)),
                    FileUtils.getFile(getCVFilePathByName(newFileName)));
        } catch (IOException e) {

        }
    }

    public static String getCVFilePathByName(String fileName) {
        return getCVFileSystemPath() + File.separator + fileName + ".pdf";
    }

    public static String getPaperFilePathByName(String fileName) {
        return getPaperFileSystemPath() + File.separator + fileName + ".pdf";
    }

    public static String getPaperFilePath(String fileName) {
        return "http://" + Settings.getInstance().getHttpServerSettings().getIp() + ":"
                + Settings.getInstance().getHttpServerSettings().getPort() + "/" + fileName + ".pdf";
    }

    public static String getCVFilePath(String oldFileName, String newFileName) {
        renameFile(oldFileName, newFileName);
        return "http://" + Settings.getInstance().getHttpServerSettings().getIp() + ":"
                + Settings.getInstance().getHttpServerSettings().getPort() + "/" + newFileName + ".pdf";
    }


}
