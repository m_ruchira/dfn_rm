package com.dfn.rm.actors;

import akka.actor.ActorRef;
import com.dfn.rm.RecrutmentManagerTestBase;
import com.dfn.rm.util.ActorName;
import org.junit.Test;

import java.time.Duration;
import java.util.concurrent.ExecutionException;

public class SupervisorTest extends RecrutmentManagerTestBase {

    @Test
    public void testRecrutementSupervisorInit() throws ExecutionException, InterruptedException {
        String supervisorPath = "/user/" +
                ActorName.RECRUITMENT_SUPERVISOR.toString();
        ActorRef supervisorRef = algoEngine.actorSelection(supervisorPath)
                                           .resolveOne(Duration.ofSeconds(5))
                                           .toCompletableFuture()
                                           .get();
        pingActor(supervisorRef);
    }

    @Test
    public void testClientSupervisorInit() throws ExecutionException, InterruptedException {
        String supervisorPath = "/user/" +
                ActorName.RECRUITMENT_SUPERVISOR.toString() +
                "/" +
                ActorName.CLIENT_SUPERVISOR.toString();
        ActorRef supervisorRef = algoEngine.actorSelection(supervisorPath)
                                           .resolveOne(Duration.ofSeconds(5))
                                           .toCompletableFuture()
                                           .get();
        pingActor(supervisorRef);
    }

    @Test
    public void testExamSupervisorInit() throws ExecutionException, InterruptedException {
        String supervisorPath = "/user/" +
                ActorName.RECRUITMENT_SUPERVISOR.toString() +
                "/" +
                ActorName.EXAMS_SUPERVISOR.toString();
        ActorRef supervisorRef = algoEngine.actorSelection(supervisorPath)
                                           .resolveOne(Duration.ofSeconds(5))
                                           .toCompletableFuture()
                                           .get();
        pingActor(supervisorRef);
    }


    private void pingActor(ActorRef actorRef) {
        // Ping the actor to check if its working.
        RecruitementActorTestKit athenaActorTestKit = new RecruitementActorTestKit(algoEngine);
        actorRef.tell("ping", athenaActorTestKit.getProbe().getRef());
        athenaActorTestKit.getProbe().expectMsg(Duration.ofSeconds(30), "ack");
    }
}
