package com.dfn.rm.bootstrap;

import com.dfn.rm.RecrutmentManagerTestBase;
import com.dfn.rm.util.SettingsNotFoundException;
import com.dfn.rm.util.SettingsReader;
import com.dfn.rm.util.settings.Settings;
import junit.framework.Assert;
import org.junit.Test;


public class AthenaSettingsReaderTest extends RecrutmentManagerTestBase {

    protected static final String DEFAULT_SETTINGS_FILE = "src/test/resources/config/SettingsTest.yml";
    @Test
    public void loadSettingsTest() throws SettingsNotFoundException {
        Settings recruitementSettings = SettingsReader.loadSettings(DEFAULT_SETTINGS_FILE);
        Assert.assertNotNull(recruitementSettings);
        Assert.assertNotNull(recruitementSettings.getClientSettings());
        Assert.assertNotNull(recruitementSettings.getClientSettings());
        Assert.assertNotNull(recruitementSettings.getDbSettings());
    }
}
